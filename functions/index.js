
'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

//------------- NOTIFICATION FUNCTION -----------------
exports.sendAdminNotification = functions.database.ref('/items/{itemid}')
    .onWrite(async (change, context) => {
     
      const itemid = context.params.itemid;
      // If message deleted exit the function.
      if (!change.after.val()) {
        return console.log('Message Deleted', itemid);
      }
      console.log('Message ID: ', itemid);

      //-------- GET MESSAGE FROM ADMIN ---------
  const fromUser = admin.database().ref(`/items/${itemid}`).once('value');

  return fromUser.then(fromUserResult => {

    const message = fromUserResult.val().body;
    const title = fromUserResult.val().title;

    console.log('Message from Admin: ', message);

      // Get the list of device notification tokens.
        const getDeviceTokensPromise = admin.database()
        .ref(`/notificationTokens`).once('value');


    
      // The array containing all the user's tokens.
      let tokens;

      return Promise.all([getDeviceTokensPromise]).then(result => {

      const tokensSnapshot = result[0];

      // Check if there are any device tokens.
      if (!tokensSnapshot.hasChildren()) {
        return console.log('There are no notification tokens to send to.');
      }
      console.log('There are', tokensSnapshot.numChildren(), 'tokens to send notifications to.');

      // Notification details.
      const payload = {
        notification: {
          title: title,
          body: message,
          icon: "default"
        }
      };

      // Listing all tokens as an array.
      tokens = Object.keys(tokensSnapshot.val());
      // Send notifications to all tokens.
      const response =  admin.messaging().sendToDevice(tokens, payload);
      // For each message check if there was an error.
      const tokensToRemove = [];
      response.results.forEach((result, index) => {
        const error = result.error;
        if (error) {
          console.error('Failure sending notification to', tokens[index], error);
          // Cleanup the tokens who are not registered anymore.
          if (error.code === 'messaging/invalid-registration-token' ||
              error.code === 'messaging/registration-token-not-registered') {
            tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
          }
        }
      });

      return Promise.all(tokensToRemove);
    });

  });

 });

// MODERATE TEXT
const capitalizeSentence = require('capitalize-sentence');
const Filter = require('bad-words');
const badWordsFilter = new Filter();

// Moderates messages by lowering all uppercase messages and removing swearwords.
exports.moderator = functions.database.ref('/Forums/{messageId}').onWrite((change) => {
  const message = change.after.val();

  if (message && !message.sanitized) {
    // Retrieved the message values.
    console.log('Retrieved message content: ', message);

    // Run moderation checks on on the message and moderate if needed.
    const moderatedMessage = moderateMessage(message.forumText);

    // Update the Firebase DB with checked message.
    console.log('Message has been moderated. Saving to DB: ', moderatedMessage);
    return change.after.ref.update({
      forumText: moderatedMessage,
      sanitized: true,
      moderated: message.forumText !== moderatedMessage,
    });
  }
  return null;
});

// Moderates the given message if appropriate.
function moderateMessage(message) {
  // Re-capitalize if the user is Shouting.
  if (isShouting(message)) {
    console.log('User is shouting. Fixing sentence case...');
    message = stopShouting(message);
  }

  // Moderate if the user uses SwearWords.
  if (containsSwearwords(message)) {
    console.log('User is swearing. moderating...');
    message = moderateSwearwords(message);
  }

  return message;
}

// Returns true if the string contains swearwords.
function containsSwearwords(message) {
  return message !== badWordsFilter.clean(message);
}

// Hide all swearwords. e.g: Crap => ****.
function moderateSwearwords(message) {
  return badWordsFilter.clean(message);
}

// Detect if the current message is shouting. i.e. there are too many Uppercase
// characters or exclamation points.
function isShouting(message) {
  return message.replace(/[^A-Z]/g, '').length > message.length / 2 || message.replace(/[^!]/g, '').length >= 3;
}

// Correctly capitalize the string as a sentence (e.g. uppercase after dots)
// and remove exclamation points.
function stopShouting(message) {
  return capitalizeSentence(message.toLowerCase()).replace(/!+/g, '.');
}